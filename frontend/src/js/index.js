import './polyfills';
import { initializeUi } from './functions/initialize';
import {subscribe} from './components/Rating';
import ProgressBar from './components/ProgressBar';

document.addEventListener('DOMContentLoaded', () => {
  const progress = document.querySelectorAll('.custom-progress');

  if(progress.length) {
    new ProgressBar(progress);
  }

  initializeUi();
  subscribe();
});
