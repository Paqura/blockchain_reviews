import radialIndicator from '../components/RadialIndicator';

export const initializeUi = () => {
  const selects = document.querySelectorAll('select');
  const tabs = document.querySelector('.tabs');
  const progressRating = document.querySelector('#indicatorContainer');

  if(window.M) {
    window.M.FormSelect.init(selects);
    window.M.Tabs.init(tabs);
  }

  // Initialize Common Rating Radial Indicator
  if(progressRating) {
    radialIndicator('#indicatorContainer', {
      barColor : '#77e27d',
      barWidth : 4,
      initValue : progressRating.dataset.rating,
      percentage: true
    });
  }
};


