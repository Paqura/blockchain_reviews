const stars = document.querySelectorAll('.star');

export const subscribe = () => {
  stars.forEach(star => {
    star.addEventListener('mouseenter', setRating);
    star.addEventListener('mouseleave', setRating);
    star.addEventListener('click', fixRating);
  });
};

const setRating = (evt) => {
  const current = evt.target.dataset.id;
  const rating = evt.target.closest('.rating');
  rating.dataset.rating = current;

  if(evt.toElement.localName !== 'svg') {
    rating.dataset.rating = 0;
    return;
  }
};

const fixRating = (evt) => {
  const currentRating = evt.target.closest('svg').dataset.id;
  const rating = evt.target.closest('.rating');
  rating.dataset.rating = currentRating;
  stars.forEach(star => {
    star.removeEventListener('mouseleave', setRating);
    star.removeEventListener('mouseenter', setRating);
  });
};
