export default class ProgressBar {
  constructor(elems) {
    this.elems = elems;
    this.lines = [];

    this.initLines();
  }

  initLines() {
    this.elems.forEach(elem => {
      this.lines.push({
        progress: elem.dataset.progress,
        node: elem.querySelector('.custom-progress-line')
      });
    });
    this.render();
  }

  render() {
    this.lines.forEach(line => {
      const progress = line.progress;
      const percent = document.createElement('div');

      percent.textContent = progress;
      percent.className = 'custom-progress-percent';

      line.node.style.width = progress;
      line.node.appendChild(percent);
    });
  }
}
